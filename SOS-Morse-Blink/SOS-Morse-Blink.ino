/*
  Blink

  The basic element of Morse code is the dot and all other elements can be defined in terms of multiples of the dot length. 
  The word PARIS is used because this is the length of a typical word in English plain text, it has a total length of 50 dot lengths. 
  If the word PARIS can be sent ten times in a minute using normal Morse code timing then the code speed is 10 WPM.
  The character speed is related to dot length in seconds by the following formula:

  Speed (WPM) = 2.4 * (Dots per second)

  Here are the ratios for the other code elements:

  Dot = basic length
  Dash length = Dot length x 3
  Pause between elements = Dot length
  Pause between characters = Dot length x 3
  Pause between words (see note) = Dot length x 7

  Note: For learning the code this ratio is often increased so that overall text speed is lower than in standard Morse code. This stretched code is called Farnsworth code.

@see http://www.nu-ware.com/NuCode%20Help/index.html?morse_code_structure_and_timing_.htm
@see https://en.wikipedia.org/wiki/Morse_code
*/

const unsigned short PIN_OUT      = 13;

const unsigned short dotLen         = 200;
const unsigned short dashLen        = dotLen*3;
const unsigned short elementPause   = dotLen;
const unsigned short characterPause = dotLen*3;
const unsigned short wordPause      = dotLen*7;

void setup()
{
  pinMode(PIN_OUT, OUTPUT);
}

void pause(unsigned short length)
{
  digitalWrite(PIN_OUT, LOW);
  delay(length);
}

void blink(unsigned short length)
{
  digitalWrite(PIN_OUT, HIGH);
  delay(length);
}

void loop()
{
  unsigned short i = 0;
  // SOS morse code: ...  ---  ...

    // S
    blink(dotLen);
    pause(elementPause);
    blink(dotLen);
    pause(elementPause);
    blink(dotLen);
    //
    pause(characterPause);
    // O
    blink(dashLen);
    pause(elementPause);
    blink(dashLen);
    pause(elementPause);
    blink(dashLen);
    //
    pause(characterPause);
    // S
    blink(dotLen);
    pause(elementPause);
    blink(dotLen);
    pause(elementPause);
    blink(dotLen);
    //
    pause(wordPause);
}
