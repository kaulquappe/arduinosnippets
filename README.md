# ArduinoSnippets
---
Small Arduino projects mostly used for testing out code.


## Examples
---
1. [SOS-Morse-Blink - Blink SOS Morse code with LED](SOS-Morse-Blink/SOS-Morse-Blink.ino)

## References
---
* http://www.nu-ware.com/NuCode%20Help/index.html?morse_code_structure_and_timing_.htm
* https://en.wikipedia.org/wiki/Morse_code

## Author:
---
* kaulquappe (https://bitbucket.org/kaulquappe)
